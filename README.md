# yaroslab_test

Test Yaroslab Odoo version 12

## Getting started


### Flujo
    - Tras la instalacion del modulo se debe ir al menu de Ajustes / Opciones Generales / Facturacion
    y ubicar "Localización Fiscal" e instalar un paquete de ubicaciones si no esta instalado ninguno y seleccionarlo.

    Consiste en crear Estudiantes, Matriculas y Cursos. Los estudiantes pueden estar en diferentes cursos inscritos, estos cursos pertenecen a una matricula especifica
    o varias de ellas. Los cursos se asocian a productos.

    El formulario de estudiantes tiene disponible la opcion de imprimir matricula, la cual es ejecutada mediante la accion "Print Enrrollment" misma que levanta un 
    asistente donde se podra seleccionar entre las matriculas a las que aplica el estudiante (Los cursos a los que aplica el estudiante estan incluidas en los cursos
    que conlleva la matricula) e imprimirlo, en este punto tambien le sera creado un recibo de pago si es que el check de "Generate Invoice" esta activo.

    Tambien esta disponible una vista para mirar los recibos, las matriculas, cursos y estudiantes a demas de los productos.


    