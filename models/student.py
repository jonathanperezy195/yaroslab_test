# -*- coding:utf-8 -*-
# import desde python

# import desde odoo
from odoo import models, fields, api, _

class Student(models.Model):
    _name = 'yar.student'
    _description = 'Students'
    _rec_name = 'partner_id'

    #-------------- fields_view_get ----------

    #-------------- default_get ----------

    #-------------- name_get ----------

    #-------------- name_search ----------

    #-------------- CRUD ----------

    #-------------- Actions -----------
    def action_open_enrrollments(self):
        return self.env.ref('yaroslab_test.yar_enrrollment_wizard_action').read()[0]
        
    
    #-------------- Metodos auxiliares Varios -----
    @api.multi
    def _compute_age(self):
        for r in self:
            r.age = 20

    # -------------- Definición de columnas ---------
    partner_id = fields.Many2one('res.partner', help='Partner id of student')

    place_birth = fields.Char(string='Place Birth', help='Place birth info')
    date_birth  = fields.Date(string='Date Birth')
    age         = fields.Integer(string='Age', compute=_compute_age, help='Age of the student')

    father_id   = fields.Many2one('res.partner', string='Father', help='Father of student')
    mother_id   = fields.Many2one('res.partner', string='Mother', help='Mother of student')
    country_id = fields.Many2one('res.country', string='Country', help='Actual country student')
    course_ids = fields.Many2many('yar.course', string='Courses', help='Courses in')

    #-------------- Constrains --------