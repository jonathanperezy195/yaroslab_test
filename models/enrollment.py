# -*- coding:utf-8 -*-
# import desde python

# import desde odoo
from odoo import models, fields, api, _

class Enrrollment(models.Model):
    _name = 'yar.enrrollment'
    _description = 'Enrrollments'

    #-------------- fields_view_get ----------

    #-------------- default_get ----------

    #-------------- name_get ----------

    #-------------- name_search ----------

    #-------------- CRUD ----------

    #-------------- Actions -----------
    
    #-------------- Metodos auxiliares Varios -----

    # -------------- Definición de columnas ---------
    name = fields.Char(string='Name')
    code = fields.Char(string='Code', help='Code of enrrollment')
    course_ids = fields.Many2many('yar.course',string='Courses', help='Courses workeds for this enrrollment')


    #-------------- Constrains --------