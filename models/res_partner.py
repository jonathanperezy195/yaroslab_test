# -*- coding:utf-8 -*-
# import desde python

# import desde odoo
from odoo import models, fields, api, _
from odoo.tools.func import default

class ResPartner(models.Model):
    _inherit = 'res.partner'

    #-------------- fields_view_get ----------

    #-------------- default_get ----------

    #-------------- name_get ----------

    #-------------- name_search ----------

    #-------------- CRUD ----------

    #-------------- Actions -----------
    
    #-------------- Metodos auxiliares Varios -----

    # -------------- Definición de columnas ---------
    gender = fields.Selection([('m','Men'), ('w','Woman'), ('o','Other')])
    is_parent = fields.Boolean(default=False)

    property_account_payable_id = fields.Many2one('account.account', required=False)
    property_account_receivable_id = fields.Many2one('account.account', required=False)

    #-------------- Constrains --------