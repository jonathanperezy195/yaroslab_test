# -*- coding:utf-8 -*-
# import desde python

# import desde odoo
from odoo import models, fields, api, _

class Courses(models.Model):
    _name = 'yar.course'
    _description = 'Course'

    #-------------- fields_view_get ----------

    #-------------- default_get ----------

    #-------------- name_get ----------

    #-------------- name_search ----------

    #-------------- CRUD ----------

    #-------------- Actions -----------
    
    #-------------- Metodos auxiliares Varios -----

    # -------------- Definición de columnas ---------
    code = fields.Char(string='Code', help='Code of course')
    type = fields.Selection([('day','Day'),('after','After'),('night', 'Night')], string='Type', help='Type of course (day, after, night)')
    duration = fields.Integer(string='Duration', default=180 ,help='Duration or time (minutos)')
    student_ids = fields.Many2many('yar.student', help='Students added into the course')
    product_id = fields.Many2one('product.product', help='Associated Product')
    price = fields.Float(related='product_id.lst_price')


    #-------------- Constrains --------