# -*- coding:utf-8 -*-
# import desde python

# import desde odoo
from odoo import models, fields, api, _
from odoo.tools.func import default

class EnrrollmentWizard(models.TransientModel):
    _name = 'yar.enrrollment.wizard'
    _description = 'Enrrollment Wizard'

    #-------------- fields_view_get ----------

    #-------------- default_get ----------

    #-------------- name_get ----------

    #-------------- name_search ----------

    #-------------- CRUD ----------

    #-------------- Actions -----------
    
    #-------------- Metodos auxiliares Varios -----
    @api.onchange('student_id')
    def onchange_student(self):
        if self.student_id:
            return {
                'domain':{'enrrollment_id':[('course_ids','in', self.student_id.course_ids.ids )]}
            }

    def get_student_default(self):
        return self._context.get('params',{}).get('id') or self._context.get('active_id')

    def print_report(self):       
        data = {
            'code': self.enrrollment_id.code,
            'name': self.enrrollment_id.name,
            'courses': [{'code': c.code, 'type':c.type, 'duration': str(c.duration)} for c in self.enrrollment_id.course_ids]
        }        

        if self.generate_invoice:            
            journal = self.env['account.journal'].search([], limit=1)
            self.env['account.invoice'].create({
                'partner_id': self.student_id.id,
                'journal_id': journal.id,
                'type': 'out_invoice',
                'date_invoice': self.create_date.date(),
                'is_receipt_payment': True,
                'state': 'draft',
                'invoice_line_ids': [(0,0,{
                    'product_id': course.product_id.id,
                    'account_id': journal.default_credit_account_id.id,
                    'name': course.product_id.name,
                    'price_unit': course.product_id.lst_price,
                }) for course in self.enrrollment_id.course_ids ]
            })

        return self.env.ref('yaroslab_test.report_enrrollment').report_action(self,data={'enrrollments':[data]})


    # -------------- Definición de columnas ---------
    student_id = fields.Many2one('yar.student', default=get_student_default, help='Active student to view')
    enrrollment_id = fields.Many2one('yar.enrrollment', help='Available enrrollments')
    generate_invoice = fields.Boolean(string='Generate Invoice', default=True, help='Genera invoice ?')

    #-------------- Constrains --------