# -*- coding: utf-8 -*-
{
    'name': "Prueba Yaroslab",

    'summary': """
        Prueba Yaroslab
    """,

    'description': """
        Module for the control of the votings structures
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'portal',
        'account',
        'snailmail_account',
        'website'
    ],


    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/student_view.xml',
        'views/enrollment_view.xml',
        'views/course_view.xml',
        'views/res_partner_view.xml',
        'views/account_invoice_view.xml',
        'views/product_product_view.xml',
        'wizard/enrrollment_wizard.xml',
        'report/enrrollment_report.xml',
    ],
    'installable':True
}